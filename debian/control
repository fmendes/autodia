Source: autodia
Section: devel
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13),
               libdbi-perl,
               libtemplate-perl,
               libxml-simple-perl,
               perl (>= 5.8.0-7)
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: http://www.aarontrevena.co.uk/opensource/autodia/
Vcs-Browser: https://salsa.debian.org/debian/autodia
Vcs-Git: https://salsa.debian.org/debian/autodia.git

Package: autodia
Architecture: all
Depends: libtemplate-perl, ${misc:Depends}, ${perl:Depends}
Suggests: libgraphviz-perl
Description: generates Dia UML diagrams from source code, XML or data
 AutoDia (Automatic Dia XML) is a modular application that parses source code,
 XML or data and produces an XML document in Dia format[1] (or images via
 graphviz and vcg).
 .
 Its goal is to be a UML/DB Schema diagram autocreation package. The diagrams
 its creates are standard UML diagrams showing dependencies, superclasses,
 packages, classes and inheritances, as well as the methods, etc of each class.
 .
 AutoDia supports any language that a Handler has been written for - see below
 for an up to date list.
 .
 Autodia now outputs the following formats:
   * Graphviz (using dot to generate jpg, png, etc)
   * dot
   * vcg
   * xvcg (using xvcg to output postscript, etc )
   * dia (using a new custom directed graph algorithm to layout diagrams)
   * HTML/XML/Anything (if you write your own template)
   * Experimental SpringGraph (native perl directed graphs similar to graphviz)
     now included
   * Experimental Umbrello XML/XMI (requires fixing)
 .
 Autodia now parses the following forms of input:
   * Perl
   * Python
   * PHP
   * Java (some issues with version 1.4) no longer fully supported (it used to
     work, Java broke its API's now it doesn't, fixes welcome)
   * C++
   * Torque (XML DB schema)
   * DBI (perl database interface handles)
   * SQL
   * Umbrello (experimental)
 .
 [1] For more information about Dia, see:
     <https://en.wikipedia.org/wiki/Dia_(software)>
